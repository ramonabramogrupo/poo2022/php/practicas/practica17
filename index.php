<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$batidora=new Electrodomestico("batidora", "braun", 2);

var_dump($batidora);

echo $batidora; // aqui llamaria al metodo __toString

echo "La batidora en 3 horas consume {$batidora->getConsumo(3)} KW";

echo "<br>La batidora en 3 horas gasta {$batidora->getCosteConsumo(3, 1)} euros";

$lavadora1=new Lavadora("balay", 2, 800, true);

var_dump($lavadora1);

$lavadora1->setPrecio(1000);
$lavadora1->setPotencia(4);
 
var_dump($lavadora1);

echo $lavadora1;

echo $lavadora1->getConsumo(10);

$lavadora1->setAguaCaliente(false);

echo "<br>";
echo $lavadora1->getConsumo(10);