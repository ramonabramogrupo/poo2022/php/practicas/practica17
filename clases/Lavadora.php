<?php

/**
 * Description of Lavadora
 *
 * @author Profesor Ramon
 */
class Lavadora extends Electrodomestico{
    public float $precio;
    public bool $aguaCaliente;
        
    public function __construct(string $marca, float $potencia,float $precio, bool $aguaCaliente) {
        // esto permite que se ejecute el constructor del padre
        // el padre es electrodomestico
        $this->aguaCaliente=$aguaCaliente;
        $this->precio=$precio;
        parent::__construct("Lavadora", $marca, $potencia);
    }
    
    public function getPrecio(): float {
        return $this->precio;
    }

    public function getAguaCaliente(): bool {
        return $this->aguaCaliente;
    }

    public function setPrecio(float $precio): void {
        $this->precio = $precio;
    }

    public function setAguaCaliente(bool $aguaCaliente): void {
        $this->aguaCaliente = $aguaCaliente;
    }
    
    
    public function getMarca(): string {
        return parent::getMarca();
    }

    public function getPotencia(): float {
        return parent::getPotencia();
    }

    public function getTipo(): string {
        return parent::getTipo();
    }

    public function setMarca(string $marca): void {
        parent::setMarca($marca);
    }

    public function setPotencia(float $potencia): void {
        parent::setPotencia($potencia);
    }

    public function setTipo(string $tipo): void {
        parent::setTipo($tipo);
    }

    public function __toString() {
        $salida=parent::__toString();
        $salida.="Precio={$this->precio}<br>";
        $salida.="Agua Caliente={$this->aguaCaliente}<br>";
        return $salida;
    }

    public function getConsumo(int $horas) {
        if($this->aguaCaliente){
            return $horas*($this->potencia+$this->potencia*0.2);
        }else{
            return $this->potencia*$horas;
        }
    }

}
