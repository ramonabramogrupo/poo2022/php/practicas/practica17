<?php

/**
 * Description of Electrodomestico
 *
 * @author Profesor Ramon
 */
class Electrodomestico {
    public string $tipo;
    public string $marca;
    public float $potencia;
    
    public function __construct(string $tipo, string $marca, float $potencia) {
        $this->tipo = $tipo;
        $this->marca = $marca;
        $this->potencia = $potencia;
    }
    
    public function getTipo(): string {
        return $this->tipo;
    }

    public function getMarca(): string {
        return $this->marca;
    }

    public function getPotencia(): float {
        return $this->potencia;
    }

    public function setTipo(string $tipo): void {
        $this->tipo = $tipo;
    }

    public function setMarca(string $marca): void {
        $this->marca = $marca;
    }

    public function setPotencia(float $potencia): void {
        $this->potencia = $potencia;
    }
    
    public function __toString() {
        $salida="<br>Tipo={$this->tipo}";
        //$salida=$salida . "Marca={$this->marca}";
        $salida.="<br>Marca={$this->marca}";
        $salida.="<br>Potencia={$this->potencia}<br>";
        return $salida;
        
    }
    
    public function getConsumo(int $horas){
        return $this->potencia*$horas;
    }
    
    public function getCosteConsumo(int $horas,float $costeHora){
        //return $this->potencia*$horas*$costeHora;
        return $this->getConsumo($horas)*$costeHora;
    }

}
